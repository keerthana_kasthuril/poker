<h3>Poker</h3>
The project includes a python based solution for determining the winning hand of Poker game. The cards are analysed and based on the priority, winner is decided. To deicde the winning hand, we need to understand the priority of hands which is listed below:<br>
1. Royal Flush - Ace, King, Queen, Jack and Ten of same suit (e.g: Ace♠, King♠, Queen♠, Jack♠, 10♠)<br>
2. Stright Flush - Cards are forming a sequence and are of same suit (e.g: 5♦, 6♦, 7♦, 8♦, 9♦)<br>
3. Four of a kind - All four cards of same value (e.g: 3♦, 3♥, 3♣, 3♠, Q♥)<br>
4. Full house - Combination of three of a kind and a pair (e.g: 8♦, 8♠, 8♣, K♣, K♦)<br>
5. Flush - All cards are of same suit and they don't need to be in order (e.g: 4♠, 9♥, 6♠, J♦, A♣)<br>
6. Straight - Similar to stright flush excpet that all cards are not of same suit (e.g: 5♦, 6♣, 7♠, 8♦, 9♠)<br>
7. Three of a kind - Three cards of same value (e.g: 8♦, 8♠, 8♣, K♣, J♦)<br>
8. Two pair - The hand consists of two sets of pair and one random card (e.g: 4♠, 4♥, 6♠, J♦, 6♣)<br>
9. Pair - Only contain one pair (e.g: 7♠, 7♥, 6♠, 2♦, 8♣)<br>
10. High card - If there is no combination like above or there is a tie, the high card value is taken into consideration<br><br>

The basic checks for all these have been implemented. In order to get the results, the cards for two players need to be given as input in the format given.<br>
The values can be any from [2,3,4,5,6,7,8,9,T,J,K,Q,A]<br>
The suit should be mentioned like H(hearts), C(Clubs), D(Diamond) and S(Spades)<br>
For one player the input should be given as : <Value><Suit> <Value><Suit> ... (5 such pairs)<br>
<b>Some sample inputs:</b><br>
2H 7C AS QD 5C  -- no ordering or same suit<br>
7S 4S 5S 3S QS  -- cards of same suit<br>
4H 5S 6H 7C 8D  -- straight <br>
AS AC KD KH KS  -- full house<br>
2S 4S 5S 3S AS  -- straight flush<br>
